# Changelog

## 0.2.0 - 2024-05-23

### Fixed

  - Fixed error when running `GetModInfo` from within a workspace

## 0.1.0 - 2024-05-22

Initial release
