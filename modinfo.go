// Package modinfo provides access to local module information.
// It's a wrapper around `go list -m -json`
package modinfo

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os/exec"
	"strings"
)

// Module describes a local module, it's a stripped down version of what's
// described in the [go command docs]
//
// [go command docs]: https://pkg.go.dev/cmd/go#hdr-List_packages_or_modules
type Module struct {
	// module path
	Path string
	// is this the main module?
	Main bool
	// directory holding local copy of files, if any
	Dir string
	// path to go.mod file describing module, if any
	GoMod string
	// go version used in module
	GoVersion string
}

// GetModInfoContext returns the local module. The context is passed to execute
// the `go list` command. If `dir` is non-empty then `go list` will be run from
// that directory.
func GetModInfoContext(ctx context.Context, dir string) (*Module, error) {
	out, err := runGoList(ctx, dir)
	if err != nil {
		return nil, err
	}

	var mod Module
	if err := json.NewDecoder(bytes.NewBuffer(out)).Decode(&mod); err != nil { //go-cov:skip
		return nil, fmt.Errorf("decoding 'go list' output: '%s': %v", string(out), err)
	}

	return &mod, nil
}

// GetModInfo is a wrapper around [GetModInfoContext] that passes
// [context.Background].
func GetModInfo(dir string) (*Module, error) {
	return GetModInfoContext(context.Background(), dir)
}

// ListModInfosContext is like [GetModInfoContext] but returns all local
// modules.
func ListModInfosContext(ctx context.Context, dir string) ([]*Module, error) {
	out, err := runGoList(ctx, dir)
	if err != nil {
		return nil, err
	}

	var modules []*Module

	decoder := json.NewDecoder(bytes.NewBuffer(out))
	for decoder.More() {
		var mod Module
		if err := decoder.Decode(&mod); err != nil { //go-cov:skip
			return nil, fmt.Errorf("decoding 'go list' output: '%s': %v", string(out), err)
		}

		modules = append(modules, &mod)
	}

	return modules, nil
}

// ListModInfos is a wrapper around [ListModInfosContext] that passes
// [context.Background].
func ListModInfos(dir string) ([]*Module, error) {
	return ListModInfosContext(context.Background(), dir)
}

func runGoList(ctx context.Context, dir string) ([]byte, error) {
	cmd := exec.CommandContext(ctx, "go", "list", "-m", "-json")
	if dir != "" {
		cmd.Dir = dir
	}
	var stderr strings.Builder
	cmd.Stderr = &stderr

	out, err := cmd.Output()
	if err != nil {
		return nil, fmt.Errorf(
			"running `%s`: %w\nstderr was: %s",
			strings.Join(cmd.Args, " "),
			err,
			stderr.String(),
		)
	}

	return out, nil
}
