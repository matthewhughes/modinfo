# `modinfo`

[![Go
Reference](https://pkg.go.dev/badge/gitlab.com/matthewhughes/modinfo.svg)](https://pkg.go.dev/gitlab.com/matthewhughes/modinfo)

Library to wrap around `go list -m`
