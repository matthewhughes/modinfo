package modinfo_test

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/mod/modfile"

	"gitlab.com/matthewhughes/modinfo"
)

func ExampleGetModInfo() {
	mod, err := modinfo.GetModInfo("")
	if err != nil {
		log.Fatalf("finding local module: %v", err)
	}

	data, err := os.ReadFile(mod.GoMod)
	if err != nil {
		log.Fatalf("reading %s: %v", mod.GoMod, err)
	}

	modFile, err := modfile.Parse(mod.GoMod, data, nil)
	if err != nil {
		log.Fatalf("parsing %s: %v", mod.GoMod, err)
	}

	fmt.Println(modFile.Go)
}

func TestGetModInfo_ErrorsonGoListFailure(t *testing.T) {
	dir := filepath.Join("testdata", "bad_mod")
	expectedErr := "running `go list -m -json`: "

	_, err := modinfo.GetModInfo(dir)

	assert.ErrorContains(t, err, expectedErr)
}

func TestGetModInfoContext_RespectsContext(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	_, err := modinfo.GetModInfoContext(ctx, "")

	assert.ErrorIs(t, err, context.Canceled)
}

func TestListModInfos_ErrorsonGoListFailure(t *testing.T) {
	dir := filepath.Join("testdata", "bad_mod")
	expectedErr := "running `go list -m -json`: "

	_, err := modinfo.ListModInfos(dir)

	assert.ErrorContains(t, err, expectedErr)
}

func TestListModInfosContext_RespectsContext(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	_, err := modinfo.ListModInfosContext(ctx, "")

	assert.ErrorIs(t, err, context.Canceled)
}

func TestGetModInfo_Success(t *testing.T) {
	absHere, err := filepath.Abs(".")
	require.NoError(t, err)
	absTestdata := filepath.Join(absHere, "testdata")
	require.NoError(t, err)

	for _, tc := range []struct {
		desc        string
		dir         string
		expectedMod *modinfo.Module
	}{
		{
			"defaults to looking in current dir",
			"",
			&modinfo.Module{
				Path:      "gitlab.com/matthewhughes/modinfo",
				Main:      true,
				Dir:       absHere,
				GoMod:     filepath.Join(absHere, "go.mod"),
				GoVersion: "1.21.0",
			},
		},
		{
			"looks in dir if given",
			filepath.Join("testdata", "other_module"),
			&modinfo.Module{
				Path:      "other_module",
				Main:      true,
				Dir:       filepath.Join(absTestdata, "other_module"),
				GoMod:     filepath.Join(absTestdata, "other_module", "go.mod"),
				GoVersion: "1.19",
			},
		},
		{
			"returns first instance from workspace",
			filepath.Join("testdata", "workspaces", "some_other_module"),
			// 'go list -m' _appears_ to return modules as they are ordered in go.work
			&modinfo.Module{
				Path:      "some_module",
				Main:      true,
				Dir:       filepath.Join(absTestdata, "workspaces", "some_module"),
				GoMod:     filepath.Join(absTestdata, "workspaces", "some_module", "go.mod"),
				GoVersion: "1.19",
			},
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			t.Run("without context", func(t *testing.T) {
				got, err := modinfo.GetModInfo(tc.dir)

				require.NoError(t, err)
				assert.Equal(t, tc.expectedMod, got)
			})
			t.Run("with context", func(t *testing.T) {
				got, err := modinfo.GetModInfoContext(context.Background(), tc.dir)

				require.NoError(t, err)
				assert.Equal(t, tc.expectedMod, got)
			})
		})
	}
}

func TestListModInfos_Success(t *testing.T) {
	absHere, err := filepath.Abs(".")
	require.NoError(t, err)
	absTestdata := filepath.Join(absHere, "testdata")
	require.NoError(t, err)

	for _, tc := range []struct {
		desc        string
		dir         string
		expectedMod []*modinfo.Module
	}{
		{
			"defaults to looking in current dir",
			"",
			[]*modinfo.Module{
				{
					Path:      "gitlab.com/matthewhughes/modinfo",
					Main:      true,
					Dir:       absHere,
					GoMod:     filepath.Join(absHere, "go.mod"),
					GoVersion: "1.21.0",
				},
			},
		},
		{
			"looks in dir if given",
			filepath.Join("testdata", "other_module"),
			[]*modinfo.Module{
				{
					Path:      "other_module",
					Main:      true,
					Dir:       filepath.Join(absTestdata, "other_module"),
					GoMod:     filepath.Join(absTestdata, "other_module", "go.mod"),
					GoVersion: "1.19",
				},
			},
		},
		{
			"returns multiple modules if available",
			filepath.Join("testdata", "workspaces"),
			[]*modinfo.Module{
				{
					Path:      "some_module",
					Main:      true,
					Dir:       filepath.Join(absTestdata, "workspaces", "some_module"),
					GoMod:     filepath.Join(absTestdata, "workspaces", "some_module", "go.mod"),
					GoVersion: "1.19",
				},
				{
					Path:      "some_other_module",
					Main:      true,
					Dir:       filepath.Join(absTestdata, "workspaces", "some_other_module"),
					GoMod:     filepath.Join(absTestdata, "workspaces", "some_other_module", "go.mod"),
					GoVersion: "1.18",
				},
			},
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			t.Run("without context", func(t *testing.T) {
				got, err := modinfo.ListModInfos(tc.dir)

				require.NoError(t, err)
				assert.Equal(t, tc.expectedMod, got)
			})

			t.Run("with context", func(t *testing.T) {
				got, err := modinfo.ListModInfosContext(context.Background(), tc.dir)

				require.NoError(t, err)
				assert.Equal(t, tc.expectedMod, got)
			})
		})
	}
}
